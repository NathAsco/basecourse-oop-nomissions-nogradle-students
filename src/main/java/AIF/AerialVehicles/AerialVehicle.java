package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.module.Module;
import AIF.module.ModuleEnum;
import AIF.module.moduleType.SingleUseModule;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AerialVehicle {

    protected boolean isInMidAir;
    protected double distSinceMaintenance;
    protected Coordinates currentLocation;
    protected ModuleEnum[] compatibleModule;
    protected List<Module> modules;

    public AerialVehicle(Coordinates currentLocation) {
        this.currentLocation = currentLocation;
        isInMidAir = false;
        distSinceMaintenance = 0;
        modules = new ArrayList<>();
    }

    public void takeOff() throws CannotPerformInMidAirException, NeedMaintenanceException {
        if (isInMidAir)
            throw new CannotPerformInMidAirException();
        else if (needMaintenance())
            throw new NeedMaintenanceException();
        else {
            isInMidAir = true;
            System.out.println("Taking off");
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (isInMidAir) {
            System.out.println("Flying to: " + destination);
            distSinceMaintenance += destination.distance(currentLocation);
            currentLocation = destination;
        } else
            throw new CannotPerformOnGroundException();
    }

    public void land() throws CannotPerformOnGroundException {
        if (isInMidAir) {
            System.out.println("Landing");
            isInMidAir = false;
        } else
            throw new CannotPerformOnGroundException();
    }

    public boolean needMaintenance() {
        return distSinceMaintenance >= getMaintenanceKm();
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (!isInMidAir) {
            System.out.println("Performing maintenance");
            distSinceMaintenance = 0;
        } else
            throw new CannotPerformInMidAirException();
    }

    public void loadModule(Module module) throws ModuleNotCompatibleException, NoModuleStationAvailableException, CannotPerformInMidAirException {
        boolean isCompatible = false;
        if (isInMidAir)
            throw new CannotPerformInMidAirException();
        if (modules.size() >= getStationNum())
            throw new NoModuleStationAvailableException();
        else {
            for (ModuleEnum moduleEnum : compatibleModule) {
                if (moduleEnum.getName().equals(module.toString()))
                    isCompatible = true;
            }

            if (isCompatible) {
                modules.add(module);
                System.out.println("Successfully loaded " + module + " module");
            } else
                throw new ModuleNotCompatibleException();
        }
    }

    public void activateModule(Class<? extends Module> moduleClass, Coordinates targetLocation) throws ModuleNotFoundException, NoModuleCanPerformException, CannotPerformOnGroundException {
        List<Module> modulesOfType = modules.stream().filter(m -> m.getClass() == moduleClass).collect(Collectors.toList());
        boolean wasActivated = false;

        if (!isInMidAir)
            throw new CannotPerformOnGroundException();
        else {
            if (modulesOfType.size() == 0)
                throw new ModuleNotFoundException();
            else {
                for (Module module : modulesOfType) {
                    if (checkIfModuleCanBeActivated(wasActivated, module, targetLocation)) {
                        module.activate(targetLocation);
                        wasActivated = true;
                    }
                }
            }
            if (!wasActivated)
                throw new NoModuleCanPerformException();
        }
    }

    public boolean checkIfDisposableAndAlreadyUsed(Module module) {
        return (SingleUseModule.class.isAssignableFrom(module.getClass()) && ((SingleUseModule) module).getIsUsed());
    }

    //Every meter equals 0.1 in the coordinates, km equals 1.0
    public boolean checkIfInRange(Coordinates targetLocation, Module module) {
        return targetLocation.distance(currentLocation) <= module.getRange();
    }

    public boolean checkIfModuleCanBeActivated(boolean wasActivated, Module module, Coordinates targetLocation) {
        return !checkIfDisposableAndAlreadyUsed(module) && checkIfInRange(targetLocation, module) && !wasActivated;
    }

    public abstract int getMaintenanceKm();

    public abstract int getStationNum();
}
