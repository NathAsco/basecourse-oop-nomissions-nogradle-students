package AIF.AerialVehicles.uav.haron;

import AIF.AerialVehicles.uav.UAV;
import AIF.Entities.Coordinates;

public abstract class Haron extends UAV {

    private static final int MAINTENANCE_KMS = 15000;

    public Haron(Coordinates currentLocation) {
        super(currentLocation);
    }

    @Override
    public int getMaintenanceKm() {
        return MAINTENANCE_KMS;
    }
}
