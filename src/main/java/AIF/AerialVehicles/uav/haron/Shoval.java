package AIF.AerialVehicles.uav.haron;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class Shoval extends Haron {

    private static final int STATION_NUM = 3;

    public Shoval(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.WEAPON, ModuleEnum.SENSOR, ModuleEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}

