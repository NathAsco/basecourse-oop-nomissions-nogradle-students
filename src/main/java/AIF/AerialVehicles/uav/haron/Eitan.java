package AIF.AerialVehicles.uav.haron;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class Eitan extends Haron {

    private static final int STATION_NUM = 4;

    public Eitan(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.WEAPON, ModuleEnum.SENSOR};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}
