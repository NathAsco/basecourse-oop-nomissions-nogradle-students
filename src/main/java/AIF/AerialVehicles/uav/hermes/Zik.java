package AIF.AerialVehicles.uav.hermes;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class Zik extends Hermes {

    private static final int STATION_NUM = 1;

    public Zik(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.SENSOR, ModuleEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}