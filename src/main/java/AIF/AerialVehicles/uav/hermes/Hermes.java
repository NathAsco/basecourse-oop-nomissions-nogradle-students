package AIF.AerialVehicles.uav.hermes;

import AIF.AerialVehicles.uav.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {

    private static final int MAINTENANCE_KMS = 10000;

    public Hermes(Coordinates currentLocation) {
        super(currentLocation);
    }

    @Override
    public int getMaintenanceKm() {
        return MAINTENANCE_KMS;
    }
}
