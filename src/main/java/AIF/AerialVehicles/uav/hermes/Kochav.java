package AIF.AerialVehicles.uav.hermes;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class Kochav extends Hermes {

    private static final int STATION_NUM = 5;

    public Kochav(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.WEAPON, ModuleEnum.SENSOR, ModuleEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}
