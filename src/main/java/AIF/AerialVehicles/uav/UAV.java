package AIF.AerialVehicles.uav;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {

    private static final int SPEED = 150;

    public UAV(Coordinates currentLocation) {
        super(currentLocation);
    }

    @Override
    public abstract int getMaintenanceKm();

    public void hoverOverLocation(Double hours) throws CannotPerformOnGroundException {
        double totalHoverDist = SPEED * hours;

        if (isInMidAir) {
            System.out.println("Hovering over: " + currentLocation);
            distSinceMaintenance += totalHoverDist;
        } else
            throw new CannotPerformOnGroundException();
    }
}
