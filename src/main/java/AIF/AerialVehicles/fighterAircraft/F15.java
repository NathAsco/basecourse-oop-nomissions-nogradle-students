package AIF.AerialVehicles.fighterAircraft;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class F15 extends FighterAircraft {

    private static final int STATION_NUM = 10;

    public F15(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.WEAPON, ModuleEnum.SENSOR};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}
