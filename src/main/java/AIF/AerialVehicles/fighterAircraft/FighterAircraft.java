package AIF.AerialVehicles.fighterAircraft;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class FighterAircraft extends AerialVehicle {

    private static final int MAINTENANCE_KMS = 25000;

    public FighterAircraft(Coordinates currentLocation) {
        super(currentLocation);
    }

    @Override
    public int getMaintenanceKm() {
        return MAINTENANCE_KMS;
    }
}
