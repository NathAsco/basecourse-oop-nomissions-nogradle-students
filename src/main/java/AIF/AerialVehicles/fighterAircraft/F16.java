package AIF.AerialVehicles.fighterAircraft;

import AIF.Entities.Coordinates;
import AIF.module.ModuleEnum;

public class F16 extends FighterAircraft {

    private static final int STATION_NUM = 7;

    public F16(Coordinates currentLocation) {
        super(currentLocation);
        compatibleModule = new ModuleEnum[]{ModuleEnum.WEAPON, ModuleEnum.BDA};
    }

    @Override
    public int getStationNum() {
        return STATION_NUM;
    }
}
