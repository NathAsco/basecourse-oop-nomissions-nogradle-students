package AIF.module.moduleType;

import AIF.Entities.Coordinates;

public class Weapon extends SingleUseModule {

    public static final Double RANGE = 1500.0;


    public Weapon() {
        super();
    }

    @Override
    public void activate(Coordinates targetLocation) {
        isUsed = true;
        System.out.println("Using weapon at coordinates: " + targetLocation);
    }

    @Override
    public Double getRange() {
        return RANGE;
    }

    @Override
    public String toString() {
        return "Weapon";
    }
}
