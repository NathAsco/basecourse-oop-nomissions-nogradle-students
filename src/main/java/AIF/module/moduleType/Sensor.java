package AIF.module.moduleType;

import AIF.Entities.Coordinates;
import AIF.module.Module;

public class Sensor implements Module {

    public static final Double RANGE = 600.0;

    @Override
    public void activate(Coordinates targetLocation) {
        System.out.println("Using sensor at coordinates: " + targetLocation);
    }

    @Override
    public Double getRange() {
        return RANGE;
    }

    @Override
    public String toString() {
        return "Sensor";
    }
}
