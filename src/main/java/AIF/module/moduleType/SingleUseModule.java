package AIF.module.moduleType;

import AIF.module.Module;

public abstract class SingleUseModule implements Module {

    protected boolean isUsed;

    public SingleUseModule() {
        isUsed = false;
    }

    public boolean getIsUsed() {
        return isUsed;
    }
}
