package AIF.module;

public enum ModuleEnum {
    WEAPON("Weapon"), BDA("BDA"), SENSOR("Sensor");

    String name;

    private ModuleEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
