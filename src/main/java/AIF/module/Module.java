package AIF.module;

import AIF.Entities.Coordinates;

public interface Module {

    public void activate(Coordinates targetLocation);

    public Double getRange();
}
